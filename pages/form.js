import React from 'react'
import { Form, Layout, Button } from 'antd'
import Page from '../layouts/Page'
import {Form as PageForm} from '../form'

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
}
const FORMAT = "YYYY/MM/DD";
const FORMAT_MONTH = "YYYY/MM";
const FORMAT_DATETIME = "YYYY/MM/DD hh:mm";

export default class Index extends React.Component {
  form = null
  formItems = [
    {
      type: 'wysiwyg',
      label: 'Description',
      onChange: (v) => {
        console.log(v)
      },
      active: false
    },
    {
      type: 'input',
      label: 'Department',
      icon: 'appstore-o',
      helptext: 'Department in which one can find this article.',
      tooltip: 'Do you know your department?',
      required: true
    },
    {
      type: 'password',
      label: 'Lock code',
      icon: 'lock',
      active: false
    },
    {
      type: 'random',
      label: 'API Key',
      icon: 'lock',
      helptext: 'click to generate key'
    },
    {
      type: 'number',
      label: 'Size',
      min: 1,
      tooltip: 'Do you know your size?'
    },
    {
      type: 'number',
      label: 'Cost',
      formatter: value => `$${value}`,
      min: 0,
      active: false
    },
    {
      type: 'checkboxgroup',
      label: 'Extras',
      options: ['Wide', 'Fitted', 'Elastic', 'Shrinkable'],
      defaultValue: ['Wide','Elastic'],
      active: false
    },
    {
      type: 'rate',
      label: 'Rating',
      value: 3.5,
      allowHalf: true,
      active: false
    },
    {
      type: 'select',
      label: 'Color',
      defaultValue: 'Blue',
      showSearch: true,
      options: [
        {label: 'Red'},
        {label: 'Green'},
        {label: 'Blue'},
      ],
      active: false
    }
  ]
  render () {
    return (
      <Page selected="sm1c" title="Using Form">
        <PageForm 
          ref={(f) => this.form = f } 
          onSubmit={(e) => this.logFormValues(e)} 
          layout="horizontal" 
          itemLayout={layout} 
          items={this.formItems}>
          <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
            <Button type="primary" htmlType="submit">Submit</Button>
          </Form.Item>
        </PageForm>
      </Page>
    )
  }
  logFormValues (e) {
    e.preventDefault();
    // console.log(this.formRef.props.form.getFieldsValue())
    console.log(this.form)
  }
} 
