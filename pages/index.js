import React from 'react'
import { Form, Button, Row, Col } from 'antd'
import Page from '../layouts/Page'
import {Form as PageForm, Select} from '../form'
import * as configs from '../form/config'

export default class Index extends React.Component {
  fields = [
    {key: 'CHECKBOX_CONFIG', value: 'checkbox', label: 'Checkbox'},
    {key: 'CHECKBOX_GROUP_CONFIG', value: 'checkboxgroup', label: 'Checkbox Group'},
  ]
  layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 14 }
  }  
  state = {
    code: '',
    fieldConfig: []
  }
  render () {
    const layout = this.layout;

    return (
      <Page selected="sm1a" title="Builder">
        <Row gutter={16}>
          <Col xs={4}>
            <PageForm>
              <Select onChange={this.showForm} placeholder="select a field type" label="Field Type" {...layout} options={this.fields} />
            </PageForm>
          </Col>
          <Col xs={10}>
            <PageForm 
              itemLayout={this.layout}
              items={this.state.fieldConfig}>
            </PageForm>
          </Col>
          <Col xs={10}>
            <div className="form-code">{this.state.code}</div>
          </Col>
        </Row>
      </Page>
    )
  }
  showForm = (v) => {
    const fieldObj = this.fields.filter(f => f.value === v)[0];
    if(fieldObj){
      const config = configs[fieldObj.key]
      if(config){
        this.setState({fieldConfig: config})
      }
    }
  }
} 
