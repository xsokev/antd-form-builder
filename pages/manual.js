import React from 'react'
import { Layout, Icon, Form } from 'antd'
import Page from '../layouts/Page'
import {Checkbox, Date, Month, DateTime, Number, Text, TextArea} from '../form'
import moment from 'moment'

const LAYOUT = {
  labelCol: { span: 4 },
  wrapperCol: { span: 14 }
}
const FORMAT = "YYYY/MM/DD";
const FORMAT_MONTH = "YYYY/MM";
const FORMAT_DATETIME = "YYYY/MM/DD hh:mm";

export default class Manual extends React.Component {
  state = {
    bool: true,
    date: moment('2017/11/17 13:45'),
    tweets: 2,
    book: 'Costs of Living',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pulvinar interdum arcu non viverra. Praesent scelerisque, justo at interdum dignissim, velit lacus rhoncus nibh, non.'
  }
  render () {
    return (
      <Page selected="sm1b">
        <div>Content</div>
        <Form layout="horizontal">
          <Checkbox 
            checked={this.state.bool} 
            label="Test Right?" 
            layout={LAYOUT}
            onCheckChange={(e) => this.setState({bool: e})} />
          <Date
            value={this.state.date} 
            label="On what date?"
            layout={LAYOUT}
            format={FORMAT}
            onChange={(d, ds) => this.setState({date: ds})} />
          <Month 
            value={this.state.date} 
            label="On what month?"
            layout={LAYOUT}
            format={FORMAT_MONTH}
            onChange={(d, ds) => this.setState({date: ds})} />
          <DateTime 
            value={this.state.date} 
            label="When?"
            layout={LAYOUT}
            format={FORMAT_DATETIME}
            onChange={(d, ds) => this.setState({date: ds})} />
          <Number 
            value={this.state.tweets} 
            label="Number of Tweets"
            layout={LAYOUT}
            min={0} max={10} step={2}
            onChange={(value) => this.setState({tweets: value})} />
          <Text 
            value={this.state.book} 
            label="Book"
            layout={LAYOUT}
            onChange={(e) => this.setState({book: e.target.value})} />
          <TextArea 
            value={this.state.description} 
            label="Description"
            layout={LAYOUT}
            autosize={true}
            onChange={(e) => this.setState({description: e.target.value})} />
        </Form>
      </Page>
    )
  }
} 
