import { action, observable } from 'mobx'

let store = null

class Store {
  @observable menuCollapsed = false

  constructor (isServer, menuCollapsed) {
    this.menuCollapsed = menuCollapsed
  }

  @action toggle = () => {
    this.menuCollapsed = !this.menuCollapsed;
    console.log(this.menuCollapsed)
  }
  @action openMenu = () => {
    this.menuCollapsed = false;
  }
  @action closeMenu = () => {
    this.menuCollapsed = true;
  }
}

export function initStore (isServer) {
  if (isServer) {
    return new Store(isServer, false)
  } else {
    // get menu state from local cache
    let menuState = false;
    if (store === null) {
      store = new Store(isServer, menuState)
    }
    return store
  }
}