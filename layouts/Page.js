import Head from 'next/head'
import { Provider, observer } from 'mobx-react'
import { LocaleProvider, Layout, Menu, Icon } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US'
import { initStore } from '../store'
import SideMenu from '../components/SideMenu'

const { Header, Content, Sider } = Layout;

@observer
export default class Page extends React.Component {
  static getInitialProps ({ req }) {
    const isServer = !!req
    const store = initStore(isServer)
    return { menuCollapsed: store.menuCollapsed, isServer }
  }
  constructor (props) {
    super(props)
    this.store = initStore(props.isServer, props.menuCollapsed)
  }
  render(){
    const {selected, title} = this.props;
    return (
      <Provider store={this.store}>
        <LocaleProvider locale={enUS}>
          <div>
            <Head>
              <meta name='viewport' content='width=device-width, initial-scale=1' />
              <meta charSet='utf-8' />
              <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/antd/2.13.4/antd.min.css' />
              {/*<link rel='stylesheet' href='/static/css/bundle.css' />*/}
              <link rel='stylesheet' href='/static/css/temp.css' />
            </Head>
            <Layout className={this.store.menuCollapsed ? 'main-layout collapsed' : 'main-layout'}>
              <SideMenu onCollapse={() => this.store.toggle()} defaultSelected={[selected]} openKey={['sm1']} collapsed={this.store.menuCollapsed} />
              <Layout>
                <Header className="header">
                  <Icon
                    className="trigger"
                    type={this.store.menuCollapsed ? 'menu-unfold' : 'menu-fold'}
                    onClick={() => this.store.toggle()}
                  />
                  <span className="page-title">{title}</span>
                </Header>
                <Content>
                  {this.props.children}
                </Content>
              </Layout>
            </Layout>
          </div>
        </LocaleProvider>
      </Provider>
    )
  }
}
