import React from 'react'
import Link from 'next/link'
import { Layout, Menu, Icon } from 'antd'
import Logo from './Logo'

const {Sider} = Layout;
const MenuItemGroup = Menu.ItemGroup;
const SubMenu = Menu.SubMenu;

const SideMenu = ({collapsed, onCollapse, defaultSelected, openKey, ...props}) => {
  let isMobile = false;
  if(global.window && global.window.innerWidth < 768){
    isMobile = true;
  }
  return (
    <Sider {...props}
      collapsed={collapsed}
      onCollapse={onCollapse}
      collapsedWidth={isMobile ? 0 : 64}
      breakpoint="sm">
      <Logo />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={defaultSelected} openKeys={openKey}>
        <SubMenu key="sm1" title={<span><Icon type="bars" /><span>Collections</span></span>}>
          <Menu.Item key="sm1a">
            <Link href="/"><span>Form Building</span></Link>
          </Menu.Item>
          <Menu.Item key="sm1b">
            <Link href="/manual"><span>Manual Creation</span></Link>
          </Menu.Item>
          <Menu.Item key="sm1c">
            <Link href="/form"><span>Form Component</span></Link>
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sm2" title={<span><Icon type="setting" /><span>Settings</span></span>}>
          <Menu.Item key="sm2a">
            <span>Users</span>
          </Menu.Item>
          <Menu.Item key="sm2b">
            <span>Profile</span>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  )
}
SideMenu.__ANT_LAYOUT_SIDER = true;

export default SideMenu;