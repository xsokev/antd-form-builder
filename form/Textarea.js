import React from 'react'
import {Form, Input} from 'antd'
import FormItem from './FormItem'

const TextArea = Input.TextArea

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  disabled, 
  placeholder,
  autosize,
  onChange, 
  onPressEnter
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <TextArea {...props} />
    </FormItem>
  )
}