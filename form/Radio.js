import React from 'react'
import {Form, Radio} from 'antd'

import FormItem from './FormItem'

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  checked, 
  defaultChecked, 
  disabled, 
  value
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <Radio {...props} />
    </FormItem>
  )
}