import React from 'react'
import {Form, Rate} from 'antd'
import moment from 'moment'

import FormItem from './FormItem'

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  count,
  value, 
  defaultValue, 
  disabled, 
  character,
  onChange, 
  onHoverChange,
  allowHalf,
  color,
  activeColor
}
*/
export default ({label, required, helptext, tooltip, layout, color, activeColor, ...props}) => {
  const style = {};
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <Rate {...props} style={style} />
    </FormItem>
  )
}