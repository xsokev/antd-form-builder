import React from 'react'
import {Form, Input, Icon} from 'antd'
import moment from 'moment'

import FormItem from './FormItem'

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  prefix, 
  suffix, 
  disabled, 
  placeholder,
  onChange, 
  onPressEnter
}
*/
export default ({label, required, helptext, tooltip, icon, prefix, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <Input type="password" prefix={icon ? <Icon type={icon} /> : prefix || null} {...props} />
    </FormItem>
  )
}