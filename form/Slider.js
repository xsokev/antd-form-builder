import React from 'react'
import {Form, Slider} from 'antd'

import FormItem from './FormItem'

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  min, 
  max,
  step,
  marks,
  dots,
  disabled, 
  range,
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <Slider {...props} />
    </FormItem>
  )
}