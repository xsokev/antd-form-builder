import React from 'react'
import {Form, Tooltip} from 'antd'

const FormItem = Form.Item

export default ({label, required, helptext, tooltip, layout, children}) => {
  return tooltip ? (
    <FormItem {...layout}
      label={label} 
      required={required}
      extra={helptext}>
      <Tooltip title={tooltip}>
        {children}
      </Tooltip>
    </FormItem>
  ) : (
    <FormItem {...layout}
      label={label} 
      required={required}
      extra={helptext}>
      {children}
    </FormItem>
)
}