import React from 'react'
import {Form, Input, Button, Menu, Dropdown, Icon} from 'antd'
import ParagraphIcon from 'react-icons/lib/fa/paragraph'
import BoldIcon from 'react-icons/lib/md/format-bold'
import ItalicIcon from 'react-icons/lib/md/format-italic'
import UnderlineIcon from 'react-icons/lib/fa/underline'
import StrikeThruIcon from 'react-icons/lib/fa/strikethrough'
import AlignLeft from 'react-icons/lib/md/format-align-left'
import AlignRight from 'react-icons/lib/md/format-align-right'
import AlignCenter from 'react-icons/lib/md/format-align-center'
import UlList from 'react-icons/lib/md/format-list-bulleted'
import OlList from 'react-icons/lib/md/format-list-numbered'
import Remove from 'react-icons/lib/fa/close'
import QuoteIcon from 'react-icons/lib/md/format-quote'
import H1Icon from 'react-icons/lib/md/looks-one'
import H2Icon from 'react-icons/lib/md/looks-two'
import H3Icon from 'react-icons/lib/md/looks-3'
import H4Icon from 'react-icons/lib/md/looks-4'
import H5Icon from 'react-icons/lib/md/looks-5'
import H6Icon from 'react-icons/lib/md/looks-6'
import FormItem from './FormItem'

const TextArea = Input.TextArea
const ButtonGroup = Button.Group

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  disabled, 
  placeholder,
  autosize,
  onChange
}
*/
export default class SimpleWysiwyg extends React.Component {
  _editor = null
  state = {
    html: ''
  }
  componentDidMount(){
    this.setState({html: (this.props.value || '')});
  }
	componentWillReceiveProps (nextProps) {
		this.setState({ html: nextProps.value });
	}
	shouldComponentUpdate (nextProps) {
    return nextProps.value !== this.state.html;
  }
  render () {
    const {label, required, helptext, tooltip, layout, disabled, onBlur} = this.props;
    
    return (
      <FormItem label={label} required={required} layout={layout} helptext={helptext} tooltip={tooltip}>
        <div>
          <ButtonGroup>
            <Button size="small" onClick={() => this._exec({key:'psp'})}>
              <ParagraphIcon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'psb'})}>
              <QuoteIcon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ps1'})}>
              <H1Icon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ps2'})}>
              <H2Icon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ps3'})}>
              <H3Icon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ps4'})}>
              <H4Icon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ps5'})}>
              <H5Icon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ps6'})}>
              <H6Icon />
            </Button>
          </ButtonGroup>
          <ButtonGroup>
            <Button size="small" onClick={() => this._exec({key:'bold'})}>
              <BoldIcon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'italic'})}>
              <ItalicIcon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'underline'})}>
              <UnderlineIcon />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'strikethru'})}>
              <StrikeThruIcon />
            </Button>
          </ButtonGroup>
          <ButtonGroup>
            <Button size="small" onClick={() => this._exec({key:'alignleft'})}>
              <AlignLeft />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'aligncenter'})}>
              <AlignCenter />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'alignright'})}>
              <AlignRight />
            </Button>
          </ButtonGroup>
          <ButtonGroup>
            <Button size="small" onClick={() => this._exec({key:'ol'})}>
              <OlList />
            </Button>
            <Button size="small" onClick={() => this._exec({key:'ul'})}>
              <UlList />
            </Button>
          </ButtonGroup>
          <ButtonGroup>
            <Button size="small" onClick={() => this._exec({key:'remove'})}>
              <Remove />
            </Button>
          </ButtonGroup>
        </div>
        <div ref={n => this._editor = n} className="editor"
          onInput={(e) => this._change(e)}
          onBlur={(e) => onBlur ? onBlur(e) : this._change(e)}
          contentEditable={!disabled} 
          dangerouslySetInnerHTML={{__html: this.state.html}}>
        </div>
      </FormItem>
    )
  }
	_change (e) {
    const {onChange} = this.props;
		const html = e.target.innerHTML;
    onChange && onChange({target: {value: html}})
	}
  _exec (e) {
    let command = '',
        arg = '';
    switch (e.key) {
      case 'psp':
        command = 'formatBlock'
        arg = 'P'
        break;
      case 'psb':
        command = 'formatBlock'
        arg = 'BLOCKQUOTE'
        break;
      case 'ps1':
        command = 'formatBlock'
        arg = 'H1'
        break;
      case 'ps2':
        command = 'formatBlock'
        arg = 'H2'
        break;
      case 'ps3':
        command = 'formatBlock'
        arg = 'H3'
        break;
      case 'ps4':
        command = 'formatBlock'
        arg = 'H4'
        break;
      case 'ps5':
        command = 'formatBlock'
        arg = 'H5'
        break;
      case 'ps6':
        command = 'formatBlock'
        arg = 'H6'
        break;
      case 'bold':
        command = 'bold'
        break;
      case 'italic':
        command = 'italic'
        break;
      case 'underline':
        command = 'underline'
        break;
      case 'strikethru':
        command = 'strikeThrough'
        break;
      case 'ol':
        command = 'insertOrderedList'
        break;
      case 'ul':
        command = 'insertUnorderedList'
        break;
      case 'alignleft':
        command = 'justifyLeft'
        break;
      case 'aligncenter':
        command = 'justifyCenter'
        break;
      case 'alignright':
        command = 'justifyRight'
        break;
      case 'alignjustify':
        command = 'justifyFull'
        break;
      case 'remove':
        command = 'removeFormat'
        break;
    }
    if(command !== ''){
      document.execCommand(command, false, arg);
    }
    this._editor && this._editor.focus();
  }
}
