import React from 'react'
import {Form, InputNumber} from 'antd'

import FormItem from './FormItem'

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  min, 
  max,
  step,
  precision,
  disabled, 
  placeholder,
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <InputNumber {...props} />
    </FormItem>
  )
}