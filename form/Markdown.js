import React from 'react'
import {Form} from 'antd'

import FormItem from './FormItem'

export default ({label, required, helptext, tooltip, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      
    </FormItem>
  )
}