import React from 'react'
import {Form, Checkbox} from 'antd'

import FormItem from './FormItem'

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  checked, 
  defaultChecked, 
  disabled, 
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} 
      tooltip={tooltip}>
      <Checkbox {...props} />
    </FormItem>
  )
}