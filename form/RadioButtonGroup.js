import React from 'react'
import {Form, Radio} from 'antd'

import FormItem from './FormItem'
const RadioGroup = Radio.Group
const RadioButton = Radio.Button

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  name,
  value, 
  defaultValue, 
  size,
  options,
  disabled, 
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, options, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <RadioGroup {...props}>
        {
          options && options.length > 0 ? options.map((opt, i) => {
            return (<RadioButton key={i} value={opt.value}>{opt.label}</RadioButton>)
          }) : null
        }
      </RadioGroup>
    </FormItem>
  )
}