import React from 'react'
import {Form, DatePicker} from 'antd'

import FormItem from './FormItem'
/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  format, 
  disabled, 
  placeholder,
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <DatePicker {...props} />
    </FormItem>
  )
}