import React from 'react'
import {Form, Input, Icon} from 'antd'
import moment from 'moment'

import FormItem from './FormItem'
const RandomGenerator = (length) => {
  var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
  var pass = "";
  for (var x = 0; x < (length || 40); x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
      if((x+1)%5 === 0 && x+1 < (length || 40)){
        pass += "-"
      }
  }
  return pass;
}
/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  prefix, 
  disabled, 
  placeholder,
  onChange
}
*/
export default class RandomInput extends React.Component {
  state = {
    value: ''
  }
  componentDidMount(){
    this.setState({value: (this.props.value || this.props.defaultValue)});
  }
  render () {
    const {label, required, helptext, tooltip, icon, prefix, layout, value, defaultValue, onChange, ...props} = this.props;
    const addOnAfter = (
      <span style={{cursor: 'default'}} onClick={() => {
        const _key = RandomGenerator()
        this.setState({value: _key})
        onChange && onChange(_key)
      }}>
        <Icon type="setting" /> Generate
      </span>
    )
    const _prefix = icon ? <Icon type={icon} /> : prefix || null;

    return (
      <FormItem layout={layout}
        label={label} 
        required={required}
        helptext={helptext} tooltip={tooltip}>
        <Input {...props} readOnly
          value={this.state.value}
          prefix={_prefix}
          addonAfter={addOnAfter} />
      </FormItem>
    )
  }
}
