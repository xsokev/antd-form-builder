import React from 'react'
import {Form, Select} from 'antd'

import FormItem from './FormItem'
const Option = Select.Option

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  mode,
  value, 
  defaultValue, 
  allowClear,
  size,
  filterOption,
  showSearch,
  options,
  disabled, 
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, options, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} tooltip={tooltip}>
      <Select {...props}>
        {
          (options && options.length > 0) ? options.map((opt, i) => {
            return (<Option key={opt.key || i} value={opt.value || opt.label || opt.key} disabled={opt.disabled}>{opt.label || opt.value}</Option>)
          }) : null
        }
      </Select>
    </FormItem>
  )
}