import React from 'react'
import {Form, Checkbox} from 'antd'

import FormItem from './FormItem'
const CheckboxGroup = Checkbox.Group

/*
{
  name,
  label,
  required,
  tooltip,
  helptext,
  value, 
  defaultValue, 
  options,
  disabled, 
  onChange
}
*/
export default ({label, required, helptext, tooltip, layout, ...props}) => {
  return (
    <FormItem layout={layout}
      label={label} 
      required={required}
      helptext={helptext} 
      tooltip={tooltip}>
      <CheckboxGroup {...props} />
    </FormItem>
  )
}