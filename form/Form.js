import React from 'react'
import {Form} from 'antd'
import {
  Checkbox,
  CheckboxGroup,
  Color,
  Date,
  DateTime,
  DateRange,
  File,
  Markdown,
  Month,
  Number,
  Password,
  Radio,
  RadioGroup,
  RadioButtonGroup,
  Random,
  Rate,
  Relational,
  Select,
  Slider,
  Text, 
  TextArea,
  Time,
  SimpleWysiwyg
} from './'

const getItem = ({type, ...props}, index) => {
  switch (type) {
    case "boolean":
    case "checkbox":
      return (<Checkbox key={index} {...props} />)
    case "checkboxgroup":
      return (<CheckboxGroup key={index} {...props} />)
    case "color":
      return (<Color key={index} {...props} />)
    case "date":
      return (<Date key={index} {...props} />)
    case "datetime":
      return (<DateTime key={index} {...props} />)
    case "daterange":
      return (<DateRange key={index} {...props} />)
    case "file":
    case "upload":
      return (<File key={index} {...props} />)
    case "markdown":
      return (<Markdown key={index} {...props} />)
    case "month":
      return (<Month key={index} {...props} />)
    case "number":
    case "numeric":
      return (<Number key={index} {...props} />)
    case "password":
      return (<Password key={index} {...props} />)
    case "radio":
      return (<Radio key={index} {...props} />)
    case "radiogroup":
      return (<RadioGroup key={index} {...props} />)
    case "radiobuttongroup":
      return (<RadioButtonGroup key={index} {...props} />)
    case "random":
      return (<Random key={index} {...props} />)
    case "rate":
    case "rating":
      return (<Rate key={index} {...props} />)
    case "relational":
      return (<Relational key={index} {...props} />)
    case "select":
    case "dropdown":
      return (<Select key={index} {...props} />)
    case "slider":
      return (<Slider key={index} {...props} />)
    case "input":
    case "text":
      return (<Text key={index} {...props} />)
    case "textarea":
      return (<TextArea key={index} {...props} />)
    case "time":
      return (<Time key={index} {...props} />)
    case "wysiwyg":
      return (<SimpleWysiwyg key={index} {...props} />)
    default:
      return null;
  }
}

class _Form extends React.Component {
  render () {
    const {items, itemLayout, children, ...props} = this.props;

    return items && items.length > 0 ? (
      <Form ref={r => this.form = r} form={this.form} {...props}>
        {
          items.filter(item => item.active !== false)
               .map(({active, ...item}, i) => {
            const Item = getItem({...item, layout: itemLayout}, i);
            return Item
          })
        }
        {children}
      </Form>
    ) : (
      <Form {...props}>{children}</Form>
    )
  }
}

// export default Form.create({
//   onFieldsChange(props, changedFields) {
//     console.log(changedFields)
//   },
//   onValuesChange(_, values) {
//     console.log(values)
//   }
// })(_Form)

export default _Form
