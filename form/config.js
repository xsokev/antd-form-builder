export const _CONFIG = [
	{
		type: 'checkbox',
		label: 'Required'
	},
	{
		type: 'text',
		label: 'Label',
		required: true
	},
]
export const CHECKBOX_CONFIG = [
	{
		type: 'text',
		label: 'Label',
		required: true
	},
	{
		type: 'text',
		label: 'Name',
		required: true,
		disabled: true
	},
	{
		type: 'text',
		label: 'Tooltip'
	},
	{
		type: 'text',
		label: 'Help Text'
	},
	{
		type: 'checkbox',
		label: 'Required'
	},
	{
		type: 'checkbox',
		label: 'Default Checked'
	},
	{
		type: 'checkbox',
		label: 'Disabled'
	},
	{
		type: 'checkbox',
		label: 'Checked',
		disabled: true
	},
	{
		type: 'textarea',
		label: 'onChange',
		disabled: true
	}
]
export const CHECKBOX_GROUP_CONFIG = [
	{
		type: 'text',
		label: 'Label',
		required: true
	},
	{
		type: 'text',
		label: 'Name',
		required: true,
		disabled: true
	},
	{
		type: 'text',
		label: 'Tooltip'
	},
	{
		type: 'text',
		label: 'Help Text'
	},
	{
		type: 'checkbox',
		label: 'Required'
	},
	{
		type: 'text',
		label: 'Label',
		required: true
	},
	{
		type: 'text',
		label: 'Options'
	},
	{
		type: 'text',
		label: 'Default Value'
	},
	{
		type: 'checked',
		label: 'Disabled'
	},
	{
		type: 'textarea',
		label: 'onChange',
		disabled: true
	}
]
